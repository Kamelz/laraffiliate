<?php namespace Vendor\Laraffiliate\Traits;

use Vendor\Laraffiliate\Services\AffiliateService;

trait Affiliateable {

	/**
	 * @param  AffiliateService 
	 */
	public static function bootAffiliateable(){
		static::saving(function ($model){
			
			$service = new AffiliateService();
			// $service->generateAffiliateId($model); TODO: debug why can't mock this method

			$affiliateCoulmn = app('config')['user_model']['coulmn'] ?? 'affiliate_id';
	
			$model->$affiliateCoulmn = $service->generateUserLink();
			if($service->hasReferral()){

				// $service->setReferral($model); TODO: debug why can't mock this method

				$affiliateModel = app()->make($service->getAffiliateModel('name'));

				$affiliate = $affiliateModel::whereId(
					$service->getRefferralIdFromURL()
				)
				->firstOrFail(); 
		
				// todo make it available for cooikes also

				$model->referred_by = $affiliate->id;
			}
        }); 
	}
}