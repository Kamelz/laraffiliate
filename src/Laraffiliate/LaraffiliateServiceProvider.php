<?php  namespace Vendor\Laraffiliate;

use Illuminate\Support\ServiceProvider;

class LaraffiliateServiceProvider extends ServiceProvider {

    /**
     * 
     * @var  string
     */
    protected $packageName = 'Laraffiliate';

    /**
     * A list of artisan commands for your package
     * 
     * @var array
     */
    protected $commands = [
   
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

        $this->loadRoutesFrom(__DIR__.'/../routes/routes.php');

        // Regiter migrations
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        // Publish your config
        $this->publishes([
            __DIR__.'/../config/laraffiliate.php' => config_path($this->packageName.'.php'),
        ], 'config');

        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/laraffiliate.php', $this->packageName
        );

    }

}
