<?php  namespace Vendor\Laraffiliate\Services;

use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AffiliateService{
	/**
	 * @param  $model
	 * @deprecated
	 * 
	 */
	public function generateAffiliateId(&$model){
		$affiliateCoulmn = app('config')['user_model']['coulmn'] ?? 'affiliate_id';
		dd(1,$model);
        $model->$affiliateCoulmn = $this->generateUserLink();
	}

	/**
	 * @param $model
	 * @deprecated
	 */
	public function setReferral(&$model){

		$affiliateModel = app()->make($this->getAffiliateModel('name'));

		$affiliate = $affiliateModel->where(
			$this->getAffiliateModel('coulmn'),
			$this->getRefferralIdFromURL()) // todo make it available for cooikes also
		->firstOrFail();

		$model->referred_by = $affiliate->id;
	}

	/**
	 * @return boolean
	 */
	public function hasReferral(): bool{
		// check url
		return strlen($this->getRefferralIdFromURL()) > 0;
		// todo check cookies
	}

	/**
	 * @return mix
	 */
	public function generateUserLink(): string{
		
		$config = $this->getConfig();
		
		$modelName =  substr($config['user_model']['name'],strrpos($config['user_model']['name'], '\\')+1);
		
		$table = Str::plural(strtolower($modelName?? 'users'));
		
		$affiliateLink = str_random(10);

		$linkExists = \DB::table($table)->where('affiliate_id',$affiliateLink)->exists();
		
		if($linkExists){
			$this->generateUserLink();
		}
		return $affiliateLink;
	}

	/**
	 * @return string
	 */
	public function getRefferralIdFromURL(){

		return (new Request())->query($this->getConfig()['url_refrence']);
	}

	/**
	 * @return array
	 */
	public function getConfig(){

		return app()->config['Laraffiliate'];
	}

	/**
	 * @param  $key
	 * @return string
	 */
	public function getUserModel($key){
		
		return $this->getConfig()['user_model'][$key];
	}

	/**
	 * @param  $key
	 * @return string
	 */
	public function getAffiliateModel($key){

		return $this->getConfig()['affiliate_model'][$key];
	}
}
