<?php 

return [
    
    /*
    |--------------------------------------------------------------------------
    | Package Configuration Option
    |--------------------------------------------------------------------------
    | Set the 2 factors of the affiliate system the affilaiter and the user
    */

    
    'affiliate_model' => [
    	'name' => \App\User::class,
    	'column' => 'affiliate_id'
    ],

    'user_model' => [
    	'name' => \App\User::class,
    	'column' => 'user_id'
    ],

   'cookies_refrence' => 'ref',
   
   'url_refrence' => 'ref',
];
