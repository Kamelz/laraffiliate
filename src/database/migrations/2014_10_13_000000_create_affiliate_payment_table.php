<?php

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
class CreateAffiliatePaymentTable extends Migration
{
    public $tableName; 
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $config = app()->config['Laraffiliate'];

        $modelName =  substr($config['user_model']['name'],strrpos($config['user_model']['name'], '\\')+1);
        
        $this->tableName = Str::plural(strtolower($modelName?? 'users'));
        $affiliateCoulmn = $config['affiliate_model']['column']??'affiliate_id';
        
        $userCoulmn = $config['user_model']['column']??'user_id';
      
        Schema::create('affiliate_payments', function (Blueprint $table) use($affiliateCoulmn , $userCoulmn) {
            $table->increments('id');

            $table->unsignedInteger($affiliateCoulmn);
            $table->unsignedInteger($userCoulmn)->unique();
            $table->boolean('paid');
            
            $table->foreign($userCoulmn)
            ->references('id')->on($this->tableName);

            $table->foreign($affiliateCoulmn)
            ->references('id')->on($this->tableName);
            
            $table->timestamps();
        });
    
    }
 
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliate_payments');
    }
}
