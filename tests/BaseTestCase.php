<?php namespace Vendor\Laraffiliate\Tests;

use Vendor\Laraffiliate\LaraffiliateServiceProvider;
use Orchestra\Testbench\TestCase;

abstract class BaseTestCase extends TestCase
{

	 /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('migrate:fresh', ['--database' => 'package_testing']);
    }


    protected function getPackageProviders($app)
    {
        return [LaraffiliateServiceProvider::class];
    }

    /**
	 * Define environment setup.
	 *
	 * @param  \Illuminate\Foundation\Application  $app
	 * @return void
	 */
	protected function getEnvironmentSetUp($app)
	{
	    $app['config']->set('database.default', 'package_testing');
	    $app['config']->set('database.connections.package_testing', [
	        'driver'   => 'mysql',
	        'username' => 'root',
	        'database' => 'package_testing',
	        'password' => '',
	        'host' => 'localhost',

	    ]);
	}

}
