<?php namespace Vendor\Laraffiliate\Tests\Unit;

use \Mockery;
use Vendor\Laraffiliate\Tests\BaseTestCase;
use Vendor\Laraffiliate\Services\AffiliateService;

class AffiliateableTraitTest extends BaseTestCase
{
    /**
     * 
     * @var $service
     */
    protected $service;

     /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->service = Mockery::mock('overload:Vendor\Laraffiliate\Services\AffiliateService')->makePartial();

        // $this->app->instance(AffiliateService::class, $this->service);
    }

   public function tearDown()
    {
        Mockery::close();
    }


    /**
     *
     * @test
     * @return void
     * 
     */
    public function it_generates_an_affiliate_id()
    {
        $this->service->shouldReceive('generateUserLink')
        ->andReturn('generated_user_link');

        $this->assertEquals('generated_user_link',$this->service->generateUserLink());
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_detects_if_user_is_referred_by_someone()
    {
        $this->service->shouldReceive('hasReferral')
        ->andReturn(true);

        $this->assertTrue($this->service->hasReferral());
    }

    /**
     *
     * @test
     * @return void
     */
    public function it_saves_referred_by_affiliate_on_saving()
    {
        $affilaite = new User();

        $this->service->shouldReceive('generateUserLink')
        ->andReturn('generated_affiliate_link');

        // $this->service->shouldReceive('generateAffiliateId')
        // ->withArgs([$affilaite]); uncomment when you debug this issue and test again
     
        $this->service->shouldReceive('hasReferral')
        ->andReturn(false);

        //we have an affiliate 
       
        $affilaite->save();

      Mockery::close();

     $userService = Mockery::mock('overload:Vendor\Laraffiliate\Services\AffiliateService')->makePartial();


        $user = new User();

        $userService->shouldReceive('generateUserLink')
        ->andReturn('generated_user_link');

        // $userService->shouldReceive('generateAffiliateId')
        // ->withArgs([$user]); uncomment when you debug this issue and test again
     

        $userService->shouldReceive('setReferral')
        ->with($user)
        ->passthru();

        $userService->shouldReceive('getAffiliateModel')
        ->once()
        ->ordered()
        ->andReturn(User::class);

        $userService->shouldReceive('hasReferral')
        ->andReturn(true);   

        $userService->shouldReceive('getRefferralIdFromURL')
        ->andReturn($affilaite->id);    
        
      
        $user->save();

         $this->assertSame($affilaite->id,$user->fresh()->referred_by);
    }
    /**
     *
     * @test
     * @return void
     */
    public function it_saves_affiliate_id_on_saving()
    {     

        $this->assertEquals(0,User::count());
        $user = new User();
        $this->service->shouldReceive('generateUserLink')
        ->andReturn('generated_affiliate_link');

        $this->service->shouldReceive('hasReferral')
        ->andReturn(false);

        $user->save();

        $this->assertNotSame("",User::first()->affiliate_id);
    }

}

namespace Vendor\Laraffiliate\Tests\Unit;
use Illuminate\Database\Eloquent\Model;
use Vendor\Laraffiliate\Traits\Affiliateable;
require_once __DIR__ . '\..\..\src\Laraffiliate\Traits\Affiliateable.php'; //todo refactor this

class User extends Model{
    
    use Affiliateable;

    public $timestamps = false;

    protected $fillable = [
        'affiliate_id',
        'referred_by'
    ];
}
