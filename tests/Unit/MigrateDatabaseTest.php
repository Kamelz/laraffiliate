<?php namespace Vendor\Laraffiliate\Tests\Unit;

use Vendor\Laraffiliate\Tests\BaseTestCase;
use Vendor\Laraffiliate\Traits\Affiliateable;

class MigrateDatabaseTest extends BaseTestCase
{

 /** @test */
    public function it_runs_the_migrations()
    {
        \DB::table('users')->insert([
            'affiliate_id' => 'axseqw213',
        ]);
        $users = \DB::table('users')->first();
        $this->assertEquals('axseqw213', $users->affiliate_id);
        
        \DB::table('affiliate_payments')->insert([
            'affiliate_id' => 1,
            'user_id' => 1,
            'paid' => '1',
        ]);

        $affiliatePayments = \DB::table('affiliate_payments')->first();
        $this->assertEquals(1, $affiliatePayments->affiliate_id);
        $this->assertEquals(1, $affiliatePayments->user_id);
        $this->assertEquals('1', $affiliatePayments->paid);
    }
}
